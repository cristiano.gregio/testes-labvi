package aluguel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class Aluguel
{
    public static double arredonda(double valor, int casas)
    {
        if (casas < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(valor);
        bd = bd.setScale(casas, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public double getValorAluguel(double valorAluguel, int dia)
    
    {
        String apiAluguel = "https://aluguebug.herokuapp.com/calc?dados={%22valor_nominal%22:" + valorAluguel + ",%22dia%22:" + dia +"}";

        try
        {
            URL url = new URL(apiAluguel);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Request-Method", "GET");
            connection.setDoInput(true);
            connection.connect();

            // Nada bonito, mas funciona :) 
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null)
            {
                content.append(inputLine);
            }
            in.close();
           
            System.out.println(content.toString().replace("\"", "").replace("\\", ""));
           
            JSONObject json = new JSONObject(content.toString().replace("\"", "").replace("\\", ""));
            double valorCalculado = json.getDouble("valor_calculado");
           
            System.out.println(arredonda(valorCalculado, 2));
           
            return arredonda(valorCalculado, 2);

        } catch (Exception exception)
        {
            exception.getMessage();
        }
        finally
        {
            
        }
        
        // A doc da API não fala nada em retorno ZERO do aluguel.
        return 0;
    }
}