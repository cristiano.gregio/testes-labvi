package aluguel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

public class AluguelTest
{
    Aluguel aluguel = new Aluguel();

    // Conforme deixei nos coment�rios do canal Unidade Testes Funcionais, meu aluguel � R$ 800,00
    static final double VALOR_NOMINAL = 800.0;

    @ParameterizedTest
    @CsvSource(value = {
     "-1;-1",
     "0;-1",
     "1;720",
     "2;720",
     "3;720",
     "4;720",
     "5;720",
     "6;760",
     "7;760",
     "8;760",
     "9;760",
     "10;760",
     "11;800",
     "12;800",
     "13;800",
     "14;800",
     "15;800",
     "16;816.8",
     "17;817.6",
    "18;818.4",
    "19;819.2",
    "20;820",
    "21;820.8",
    "22;821.6",
    "23;822.4",
    "24;823.2",
    "25;824",
    "26;824.8",
    "27;825.6",
    "28;826.4",
    "29;827.2",
    "30;828",
     "31;-1"
    }, delimiter = ';')
    public void testCSVPassingByValueCGN(int dia, double valor)
    {
        double valorAluguel = aluguel.getValorAluguel(VALOR_NOMINAL, dia);
        assertEquals(valorAluguel, valor);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/dia_valor.csv", delimiter = ';')
    public void testCSVExternoCGN(int dia, double valor)
    {
        double valorAluguel = aluguel.getValorAluguel(VALOR_NOMINAL, dia);
        assertEquals(valorAluguel, valor);
    }
}
